const path = require("path");

const config = {
    entry: ["./src/index.js"],
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "main.js"
    },
    module: {
        rules: [
            {
                //The babel loader is needed for interpreting JSX and transpiling the code to ES5
                test: /\.js$/,
                loader: "babel-loader",
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"]
                }
            },
            {
                //This loader is for the SVG arrow file
                test: /\.svg$/,
                loader: "svg-url-loader",
                options: {
                    limit: 10000
                }
            }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, "build"),
        compress: true,
        port: 3000
    },
    devtool: "source-map"
}

module.exports = config;