import styled from "styled-components";

//The wrapper for all of the slides - it is one big rectangle that
//has the width of all of the slides combined
//it is hidden out of view, because the carousel container has overflow: hidden
//after pressing "next slide", the whole rectangle moves, creating the effect of a slideshow

//Also, the first style object controls smooth scrolling
const SlideContainer = styled.div.attrs(props => ({
    style: {
        transform: props.touching && `translateX(${((props.touchMovement / 2) - (props.currentSlide * props.scrollOffset))}px)`,
        transitionDuration: props.touching && "0s",
    }
}))`
    /* Dimensions */
    width: ${props => props.length * 100}%;
    height: 100%;
    /* Visuals */
    display: flex;
    transform: translateX(-${props => props.currentSlide * props.scrollOffset}px);
    transition: transform 0.3s;
    transition-timing-function: "ease";
    }
    
    /*I moved the stylings from the Slide component here to eliminate the need of the Slide component
    These styles are for the slides themselves */
    & * {
        /* Dimensions */
        width: 100%;
        /* Visuals */
        display: flex;
        flex: 1;
        justify-content: center;
        text-align: center;
        max-height: 100%;
        /* I preferred preserving the aspect ratios of the images */
        object-fit: contain;
        word-wrap: break-word;
        text-overflow: ellipsis;

        &:hover {
            cursor: grab;
        }
    }
    }
`

export default SlideContainer;