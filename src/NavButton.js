import styled from "styled-components";

//Nav Buttons at the bottom
const NavButton = styled.button`
    /* Positioning */
    position: absolute;
    top: 90%;
    left: ${props => (50 - props.num * 2) + props.id * 4}%;
    /* Dimensions */
    /* We have constant button size, to make them appear bigger on smaller screens */
    width: 1rem;
    height: 1rem;
    /* Visuals */
    border: 2px solid black;
    border-radius:50%;
    opacity: 50%;
    outline: none;
    background: ${props => props.highlight == true ? 'black' : 'none'};
    transition: background 0.3s;
    -webkit-tap-highlight-color: transparent;

    /* Re-Centers the buttons for smaller screens */
    @media(max-width: 700px) {
        top: 85%;
        left: ${props => (50 - props.num * 4) + props.id * 8}%;
    }

    &:hover {
        cursor: pointer;
        opacity: 100%;
    }
`

export default NavButton;