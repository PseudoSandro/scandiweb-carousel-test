import styled from "styled-components";

//The outermost container, that serves as the "frame" of the slideshow
const CarouselContainer = styled.div`
    /* Positioning */
    position: relative;
    margin: 0 auto;
    /* Dimensions */
    width: 50%;
    height: 40vh;
    /* Visuals */
    overflow: hidden;
    background: rgb(255, 226, 224);
    @media (max-width: 1024px) {
        width: 70vw;
        height: 40vw;
    }
`
export default CarouselContainer