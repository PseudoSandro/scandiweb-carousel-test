//The main carousel component that houses all the logic

import React, { useState, useEffect, useRef } from "react"
import CarouselContainer from "./CarouselContainer"
import SlideContainer from "./SlideContainer"
import NavButton from "./NavButton"
import NavArrow from "./NavArrow"

//this is the svg image that is used in the arrows
import next from "../arrowIcon/next.svg"

const Carousel = ({ children }) => {

    // --------------------------STATE-----------------------------------------

    //State for knowing the index of the current slide
    const [slideIndex, setSlideIndex] = useState(0);

    //To achieve the infinite effect, a new copy of the slides array is added everytime the last slide is reached
    //This state holds the current slide's position in that big array that contains
    //the copies 
    const [currentSlide, setCurrentSlide] = useState(0);

    //Holds the width of one slide (each slide has the same width)
    //This state is used to calculate how much distance to move, to get to the next slide
    const [offset, setOffset] = useState(0);

    //These states hold info for - First touch position, current touch position and whether
    //The user is touching at the moment or not
    const [touchStartPosition, setTouchStartPosition] = useState(0);
    const [touchCurrentPosition, setTouchCurrentPosition] = useState(0);
    const [touching, setTouching] = useState(false);

    //Holds the slides to show
    const [slides, setSlides] = useState(children)

    //This holds the transparent image that replaces the dragging "ghost" image
    //I did not want to put it outside of state, because it would create the image on every render
    //This way, the image is created only once using an useEffect hook.
    //Also, creating this image in the dragStart event did not work, because it would be late
    //and the ghost image would still appear on first drag, so this was the optimal solution
    const [dragImage, setDragImage] = useState(false)

    //--------------------------REF---------------------
    //We get the individual slide's width using this ref
    const myRef = useRef(null);

    //-------------------------METHODS------------------
    const nextSlide = () => {
        setCurrentSlide(current => current + 1);
    }

    const prevSlide = () => {
        setCurrentSlide(current => current - 1);
    }

    //This method is made for the navigation buttons at the bottom
    //It is used to calculate which slide to go to after pressing them
    //It is a bit complicated because to achieve the infinite effect,
    //We work with a compound array made of many copies of the same slides array
    const goToSlide = index => {
        let offset = 0;
        if (children.length == slides.length) {
            offset = 0;
        } else if (index == children.length - 1) {
            offset = 0;
        } else {
            offset = slides.length - children.length;
        }
        let goTo = index + offset;
        setCurrentSlide(goTo);
    }

    // ----------------------------------------
    //TOUCH EVENT HANDLERS
    // --------------------------------------------

    //Fires when the user starts touching the screen
    const touchStart = e => {
        setTouchStartPosition(e.touches[0].clientX)
        setTouching(true);
    }

    //Fires off when the user is no longer touching the screen
    const touchEnd = e => {
        setTouching(false);
        setTouchStartPosition(0);
        setTouchCurrentPosition(0);

        //This controls the swipes - if the delta between start touch position and current touch position
        //exceeds a threshold, we move on to the next slide
        if (currentSlide !== 0 && touchCurrentPosition !== 0 && touchCurrentPosition - touchStartPosition > e.target.parentElement.clientWidth / 3) {
            setCurrentSlide(current => current - 1);
        } else if (touchCurrentPosition !== 0 && touchCurrentPosition - touchStartPosition < - e.target.parentElement.clientWidth / 3) {
            setCurrentSlide(current => current + 1)
        }
    }

    //Sets state equal to current touch position
    const touchMove = e => {
        let currentTouch = e.touches[0].clientX;
        setTouchCurrentPosition(currentTouch);
    }

    // --------------------------------------------
    //MOUSE DRAG EVENT HANDLERS
    // --------------------------------------------

    //Fires when the user starts dragging using the cursor
    //Also it removes the "ghost" image when dragging, by replacing it with the
    //transparent image held in the state upon initialization
    const mouseStart = e => {
        if (dragImage) {
            e.dataTransfer.setDragImage(dragImage, 0, 0);
        }
        setTouchStartPosition(e.screenX);
        setTouching(true);
    }

    //This controls the swipes - if the delta between start touch position and current touch position
    //exceeds a threshold, we move on to the next slide
    const mouseStop = e => {
        if (currentSlide !== 0 && touchCurrentPosition !== 0 && touchCurrentPosition - touchStartPosition > e.target.parentElement.clientWidth / 3) {
            setCurrentSlide(current => current - 1);
        } else if (touchCurrentPosition !== 0 && touchCurrentPosition - touchStartPosition < - e.target.parentElement.clientWidth / 3) {
            setCurrentSlide(current => current + 1)
        }

        setTouching(false);
        setTouchStartPosition(0);
        setTouchCurrentPosition(0);
    }

    //Sets state equal to current touch position
    const dragMove = e => {
        if (e.screenX !== 0) {
            setTouchCurrentPosition(e.screenX);
        }
    }

    //-----------------------------USEEFFECT HOOKS---------------------------------------------
    //When reaching the final slide, this attaches another copy of the slides array to the slides state
    //and after pressing NEXT the first slide appears, giving us the infinite effect
    useEffect(() => {
        if (currentSlide === slides.length - 1) {
            let neededSlides = [];
            for (let i = 0; i < children.length; i++) {
                neededSlides.push(children[i]);
            }
            setSlides(slides => [...slides, ...neededSlides])
        }
    }, [currentSlide])

    //This tells us what is the index of the current slide
    useEffect(() => {
        let index = currentSlide % children.length;
        setSlideIndex(index);
    }, [currentSlide])

    //We get the individual slides width for a given screen using this method
    useEffect(() => {
        let off = myRef.current.clientWidth / myRef.current.childElementCount;
        setOffset(off);
    }, [currentSlide])

    //This creates a placeholder transparent image to take place of the original
    //image thumbnail when dragging - the "ghost" image disappears, because this image is transparent
    useEffect(() => {
        let image = new Image();
        image.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
        setDragImage(image);
    }, [])

    return (
        <>
            {/* This component serves as the "frame" of the carousel, with overflow: hidden */}
            <CarouselContainer>
                {/* And this component holds all of the slides itself */}
                <SlideContainer
                    length={slides ? slides.length : 0}
                    currentSlide={currentSlide}
                    //touchMovement equals to how much the touching finger has moved
                    touchMovement={touchStartPosition != 0 && touchCurrentPosition != 0 && touchCurrentPosition - touchStartPosition}
                    touching={touching}
                    scrollOffset={offset}
                    ref={myRef}
                    onTouchStart={touchStart}
                    onDragStart={mouseStart}
                    onTouchMove={touchMove}
                    onDrag={dragMove}
                    onTouchEnd={touchEnd}
                    onDragEnd={mouseStop}
                    onDragOver={e => e.preventDefault()}
                    draggable='true'
                >
                    {slides}
                </SlideContainer>
                <NavArrow
                    dir="left"
                    onClick={prevSlide}
                    hidden={currentSlide === 0 && true}
                >
                    <img src={next}></img>
                </NavArrow>
                <NavArrow
                    dir="right"
                    onClick={nextSlide}
                >
                    <img src={next}></img>
                </NavArrow>
                {children.map((item, index) => {
                    return <NavButton
                        id={index}
                        key={index}
                        num={children.length}
                        highlight={slideIndex == index ? true : false}
                        onClick={() => goToSlide(index)}>
                    </NavButton>
                })}
            </CarouselContainer>
        </>
    )
}

export default Carousel;