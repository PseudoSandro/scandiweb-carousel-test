import styled from "styled-components";

//The arrow for navigation
const NavArrow = styled.button`
    /* Positioning */
    display: inline-block;
    position: absolute;
    top: 42%;
    left: ${props => props.dir == "left" ? 6 : null}%;
    right: ${props => props.dir == "right" ? 6 : null}%;
    /* Dimensions and Shape */
    width: 7%;
    transform: ${props => props.dir == "left" ? "rotate(180deg)" : null};
    /* Visuals */
    outline: none;
    border: none;
    background: none;
    color: rgb(255, 73, 61);
    opacity: 50%;
    -webkit-tap-highlight-color: transparent;

    &:hover {
        cursor: pointer;
        opacity: 100%;
    }

    & svg {
        width: 50px;
        height: 50px;
    }
`

export default NavArrow;