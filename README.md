# Carousel Component

Hello!

This Readme will tell you what i used to create the carousel and how to install & use it.
## Features

 - Swipes and smooth finger-following animation (use the Chrome Developer Tools  Device Toolbar to test touch functionality)
 - Infinite scrolling
 - Supports multiple Slides on Screen
 - Buttons for scrolling to a desired slide
 - Is Responsive
 - Works for any HTML content

## Technologies Used

 - **Webpack** for bundling React with Babel and etc.
 - **NPM**
 - **React & React-DOM** 
 - **Styled-Components** for CSS-in-JS

## Installation

 1. Move into the "scandiweb-carousel" folder using a terminal
 2. Enter `npm install`
 3. After the project is installed, entering `npm start` will launch a development server on localhost:3000 with a Demo Application

# Using the Component
In the DemoApp, you can use the main component - `<Carousel />` and place slides in it using `<div>` tags.

 - To Create an empty Carousel, use the `<Carousel></Carousel>` Tags.
 - Inserting a slide is achieved using `<div>` tags - simply place content encircled by `<div>` tags inside the `<Carousel />` tag. 
 -  To insert multiple Slides in a single slide, simply place each slide in its own separate `<div>` tag inside the main slide `<div>` tag.
