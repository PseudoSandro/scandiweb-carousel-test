import React from "react"
import Carousel from "./src/Carousel"

const DemoApp = () => {
    return (
        <Carousel>
            <div>
                <img src="https://images.unsplash.com/photo-1459682687441-7761439a709d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=810&q=80"></img>
            </div>
            <div>
                <img src="https://images.unsplash.com/photo-1580063569926-e329e888523e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"></img>
            </div>
            <div>
                <img src="https://images.unsplash.com/photo-1465153690352-10c1b29577f8?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=658&q=80"></img>
            </div>
            <div>
                <h3>Duck is the common name for numerous species in the waterfowl family Anatidae which also includes swans and geese.</h3>
            </div>
            <div>
                <div>
                    <img src="https://images.unsplash.com/photo-1514017458933-5704f36be78b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80"></img>
                </div>
                <div>
                    <img src="https://images.unsplash.com/photo-1516798087379-030749c25534?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"></img>
                </div>
            </div>
            <div>
                <div>
                    <img src="https://images.unsplash.com/photo-1557769956-79f4c40fa20e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"></img>
                </div>
                <div>
                    <img src="https://images.unsplash.com/photo-1593753149809-12ca35ab4c3d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"></img>
                </div>
            </div>
        </Carousel>
    )
}

export default DemoApp;